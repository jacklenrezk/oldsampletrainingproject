﻿using SchoolWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolWebApplication.Shared;

namespace SchoolWebApplication.Controllers
{
    public class MainReportsController : Controller
    {
        private SchoolEntities1 db = new SchoolEntities1();
        
        // GET: /MainReports/
        public ActionResult MainReportsIndex()
        {
            ViewData["YearValues"] = new SelectList(Enumerable.Range(2010, (DateTime.Now.Year - 2010 + 1)));
            ViewData["ClassesValues"] = new SelectList("");

            return View();
        }

        [HttpPost]
        public JsonResult GetClasses(int year)
        {
            var classses = db.Classes.Where(x => x.Year == year).ToList();
            if (classses != null) ViewData["ClassesValues"] = classses;
            
            List<SchoolWebApplication.Shared.BLL.MyClassList> SelectedYearClasses = new List<SchoolWebApplication.Shared.BLL.MyClassList>();
            foreach(var cls in classses)
            {
                var listOfClss = new SchoolWebApplication.Shared.BLL.MyClassList();
                listOfClss.ClasID = cls.ClassId;
                listOfClss.Title = cls.Title;
                listOfClss.StudentsNum = cls.StudentsNum;
                listOfClss.TeacherName = cls.Teacher.Name;

                SelectedYearClasses.Add(listOfClss);
            }

            return Json(SelectedYearClasses);
        }

        [HttpPost]
        public JsonResult GetStudents(int ClassId)
        {
            var SelectedClas = db.Classes.Find(ClassId);
            var students = SelectedClas.Students.ToList();
            List<SchoolWebApplication.Shared.BLL.MyStudentList> ChoosenStudents = new List<SchoolWebApplication.Shared.BLL.MyStudentList>();
            foreach(var stud in students)
            {
                var ListOfStuds = new SchoolWebApplication.Shared.BLL.MyStudentList();
                ListOfStuds.StudID = stud.StudentID;
                ListOfStuds.Name = stud.Name;
                ListOfStuds.City = stud.City;
                ListOfStuds.Phone = stud.Phone;
                ListOfStuds.GPA = Convert.ToDecimal(stud.GPA);

                ChoosenStudents.Add(ListOfStuds);
            }

            return Json(ChoosenStudents);
        }
	}
}