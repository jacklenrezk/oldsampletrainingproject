﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolWebApplication.Models;
using System.Data.Entity;

namespace SchoolWebApplication.Controllers
{
    public class MainStudentController : Controller
    {
        private SchoolEntities1 db = new SchoolEntities1();
        //
        // GET: /MainStudent/
        public ActionResult MainStudentsIndex(int? UserId, int? Flag)
        {
            if(Session["LoggedUserFullName"]!=null)
            {
                if(UserId !=null)
                {
                    var user = db.Users.Find(UserId);
                    Session["LoginUser"] = "true";
                    Session["LoggedUserID"] = user.UserId;
                    Session["LoggedUserFullName"] = user.FullName;
                }
                if (Flag != null)
                {
                    ViewBag.DoubleLogin = "You are already logged in as " + Session["LoggedUserFullName"];
                }
                else
                {
                    ViewBag.DoubleLogin = null;
                }
                var students = db.Students.ToList();
                return View(students);
            }
            else
            {
                return RedirectToAction("MainLog", "Home");
            }
        }

        [HttpPost]
        public JsonResult AddStudent(string Name, string City, string Phone,decimal? GPA)
        {
            try
            {
                if (Name != "" && City != "" && Phone != "")
                {
                    var newStudent = new Student();
                    newStudent.Name = Name;
                    newStudent.City = City;
                    newStudent.Phone = Phone;
                    if (GPA > 4 || GPA < 0) { return Json(new { status = false, message = "Invalid GPA number" }); } else { newStudent.GPA = GPA; }
                    
                    db.Students.Add(newStudent);
                    db.SaveChanges();

                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }

        }

        public ActionResult MainStudentsGridPartialView()
        {
            
            var students = db.Students.ToList();
            return PartialView(students);
        }

        [HttpPost]
        public JsonResult Editstudent(int StudentId,string Name,string City,string Phone,string GPA)
        {
            try
            {
                if (Name != "" && City != "" && Phone != "")
                {
                    var NewStudent = db.Students.Find(StudentId);
                    NewStudent.Name = Name;
                    NewStudent.City = City;
                    NewStudent.Phone = Phone;
                    NewStudent.GPA = Convert.ToDecimal(GPA);
                    db.Entry(NewStudent).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        [HttpGet]
        public JsonResult GetStudentData(int StudentId)
        {
            var SelectedStudent = db.Students.Find(StudentId);

            Student studentInfo = new Student
            {
                Name = SelectedStudent.Name,
                City = SelectedStudent.City,
                Phone = SelectedStudent.Phone,
                GPA=SelectedStudent.GPA
            };
            return Json(studentInfo, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteStudent(int StudentId)
        {
            try
            {
                if (StudentId != 0)
                {
                    var dataStudent = db.Students.Find(StudentId);
                    foreach (var clas in dataStudent.Classes.ToList())
                    {
                        dataStudent.Classes.Remove(clas);
                        db.SaveChanges();
                    }
                    db.Students.Remove(dataStudent);
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "please try again" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }
	}
}