﻿using SchoolWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolWebApplication.Shared;

namespace SchoolWebApplication.Controllers
{
    public class MainReportsController : Controller
    {
        private SchoolEntities2 db = new SchoolEntities2();
        
        // GET: /MainReports/
        public ActionResult MainCourseReportsIndex()
        {
            //ViewData["YearValues"] = new SelectList(Enumerable.Range(2010, (DateTime.Now.Year - 2010 + 1)));
            ViewData["CoursesTiltles"] = new SelectList(db.Courses, "CourseId", "Title");
            ViewData["StudentsValues"] = new SelectList("");

            return View();
        }

        [HttpPost]
        public JsonResult GetCourses(int courseId)
        {
            try
            {
                List<Student> ListStudents = new List<Student>();
                var coursess = db.Courses.ToList();
                var CourseDetails = new SchoolWebApplication.Shared.BLL.MyCourseClass();
                var selectedCourse = db.Courses.Find(courseId);
                CourseDetails.CourseId = selectedCourse.CourseId;
                CourseDetails.Title = selectedCourse.Title;
                CourseDetails.CreditHours = selectedCourse.CreditHours;
                CourseDetails.TeacherName = selectedCourse.Teacher.Name;
                CourseDetails.ClassTitle = selectedCourse.Class.Title;


                //foreach(var cls in coursess)
                //{
                foreach (var item in selectedCourse.FK_CourseStudent)
                {
                    var student = db.Students.Find(item.StudentId);
                    ListStudents.Add(student);
                   // CourseDetails.AssignedStudents = new List<Student>();
                    //studen.StudentId = student.StudentId;
                    //studen.Name = student.Name;
                    //studen.City = student.City;
                    //studen.Phone = student.Phone;

                    //CourseDetails.AssignedStudents.Add(student);
                }
                return Json(new { stList = new SelectList(ListStudents, "StudentId", "Name"), CourseDetails = CourseDetails });
                //return Json(CourseDetails);
            }
            catch(Exception e)
            {
                return Json(e);
            }
            //}

            //if (coursess != null) ViewData["StudentsValues"] = ListStudents;
            //List<SchoolWebApplication.Shared.BLL.MyCourseList> SelectedCourses = new List<SchoolWebApplication.Shared.BLL.MyCourseList>();
            //foreach (var cours in coursess)
            //{
            //    var listOfCourses = new SchoolWebApplication.Shared.BLL.MyCourseList();
            //    listOfCourses.CourseId = cours.CourseId;
            //    listOfCourses.Title = cours.Title;
            //    listOfCourses.CreditHours = cours.CreditHours;
            //    listOfCourses.TeacherName = cours.Teacher.Name;
            //    listOfCourses.ClassTitle = cours.Class.Title;

            //    SelectedCourses.Add(listOfCourses);
            //}
            //var selectedCourse = db.Courses.Find(courseId);

            
        }

        [HttpPost]
        public JsonResult GetStudentDetails(int StudentId)
        {
            var SelectedStudent = db.Students.Find(StudentId);
            //var students = SelectedClas.Students.ToList();
            List<SchoolWebApplication.Shared.BLL.MyStudentClass> ChoosenStudents = new List<SchoolWebApplication.Shared.BLL.MyStudentClass>();
            //foreach(var stud in students)
            //{
            //    var ListOfStuds = new SchoolWebApplication.Shared.BLL.MyStudentList();
            //    ListOfStuds.StudID = stud.StudentID;
            //    ListOfStuds.Name = stud.Name;
            //    ListOfStuds.City = stud.City;
            //    ListOfStuds.Phone = stud.Phone;
            //    ListOfStuds.GPA = Convert.ToDecimal(stud.GPA);

            //    ChoosenStudents.Add(ListOfStuds);
            //}
            List<BLL.MyCourseClass> coursesForSelectedStudent =new List<BLL.MyCourseClass>();
            var courses=db.Courses.ToList();
            foreach(var cors in courses)
            {
                foreach(var stud in cors.FK_CourseStudent)
                {
                    if (stud.StudentId==StudentId)
                    {
                        var newCors=new BLL.MyCourseClass();
                        newCors.CourseId=cors.CourseId;
                        newCors.Title=cors.Title;
                        newCors.ClassTitle=cors.Class.Title;
                        newCors.CreditHours=cors.CreditHours;
                        newCors.TeacherName=cors.Teacher.Name;
                        coursesForSelectedStudent.Add(newCors);
                    }
                }
            }
            
            return Json(coursesForSelectedStudent);
        }

        [HttpPost]
        public JsonResult GetDataForSearchedCreditHours(decimal val)
        {
            List<BLL.MyCourseClass> coursesForSelectedCreditHours = new List<BLL.MyCourseClass>();
            var courses = db.Courses.ToList();
            foreach (var Cors in courses)
            {
                if (val == Cors.CreditHours)
                {
                    var newCors = new BLL.MyCourseClass();
                    newCors.CourseId = Cors.CourseId;
                    newCors.Title = Cors.Title;
                    newCors.ClassTitle = Cors.Class.Title;
                    newCors.CreditHours = Cors.CreditHours;
                    newCors.TeacherName = Cors.Teacher.Name;
                    coursesForSelectedCreditHours.Add(newCors);
                }
            }

            return Json(coursesForSelectedCreditHours);
        }

        public ActionResult MainStudentReportsIndex()
        {
            ViewData["AllStudents"] = new SelectList(db.Students, "StudentId", "Name");
            ViewData["CoursesTitles"] = new SelectList("");
            return View();
        }

        [HttpPost]
        public JsonResult GetStudents(int StudentId)
        {
            try
            {
                List<Course> ListCourses = new List<Course>();
                var listOfStudentsCourseGrades = new List<BLL.StudentTranscript>();
                var students = db.Students.ToList();
                var StudentDetails = new SchoolWebApplication.Shared.BLL.MyStudentClass();
                var selectedStudent = db.Students.Find(StudentId);
                StudentDetails.StudID = selectedStudent.StudentId;
                StudentDetails.Name = selectedStudent.Name;
                StudentDetails.City = selectedStudent.City;
                StudentDetails.Phone = selectedStudent.Phone;
                
                foreach(var item in selectedStudent.FK_CourseStudent)
                {
                    var sttranscript = new BLL.StudentTranscript();
                    sttranscript.StudId = item.StudentId;
                    sttranscript.Course = item.Course.Title;
                    sttranscript.CreditHours = item.Course.CreditHours;
                    sttranscript.GradePoints = Convert.ToDecimal(item.CourseGrade);
                    listOfStudentsCourseGrades.Add(sttranscript);

                }
                decimal SumPoint=0;
                int SumCreditHours = 0;
                foreach(var stdData in listOfStudentsCourseGrades)
                {
                    SumPoint += stdData.GradePoints;
                    SumCreditHours += stdData.CreditHours;
                }
                var GpaDecimalVal = (SumPoint / SumCreditHours);
                var gpaVals=db.GPAs.ToList();
                foreach(var gpa in gpaVals)
                {
                    if(GpaDecimalVal<=gpa.MaxGrade && GpaDecimalVal>=gpa.MinGrade)
                    {
                        StudentDetails.CumulativeGPA = gpa.Gpa1;
                        break;
                    }
                }
                //StudentDetails.CumulativeGPA =



                List<BLL.CourseDetails> StudentDetailsList = new List<BLL.CourseDetails>();

                foreach (var item in selectedStudent.FK_CourseStudent)
                {
                    var cours = db.Courses.Find(item.CourseId);
                    ListCourses.Add(cours);
                    var CourseItemDetailed = new BLL.CourseDetails();
                    CourseItemDetailed.CourseTitle = item.Course.Title;
                    CourseItemDetailed.CourseGrade = Convert.ToDecimal(item.CourseGrade);
                    CourseItemDetailed.Semester = item.Semester;
                    CourseItemDetailed.ClassForCourse = item.Course.Class.Title;
                    CourseItemDetailed.TeacherForCourse = item.Course.Teacher.Name;
                    StudentDetailsList.Add(CourseItemDetailed);

                    // CourseDetails.AssignedStudents = new List<Student>();
                    //studen.StudentId = student.StudentId;
                    //studen.Name = student.Name;
                    //studen.City = student.City;
                    //studen.Phone = student.Phone;

                    //CourseDetails.AssignedStudents.Add(student);
                }
                return Json(new { corsList = new SelectList(ListCourses, "CourseId", "Title"), StudentDetailsList = StudentDetailsList, StudentDetails = StudentDetails });
                //return Json(CourseDetails);
            }
            catch (Exception e)
            {
                return Json(e);
            }
        }

        [HttpPost]
        public JsonResult GetStudentForCourse(int CourseId)
        {
            var course = db.Courses.Find(CourseId);
            var StudListForSelectedCourse = new List<BLL.MyStudentClass>();
            foreach(var stud in course.FK_CourseStudent)
            {
                var st=new BLL.MyStudentClass();
                st.StudID= stud.StudentId;
                st.Name= stud.Student.Name;
                st.City= stud.Student.City;
                st.Phone= stud.Student.Phone;
                StudListForSelectedCourse.Add(st);
            }
            return Json(StudListForSelectedCourse);
        }

        [HttpPost]
        public JsonResult GetDataForSearchedSemester(string semester)
        {
            List<BLL.MyStudentClass> studentsForSelectedSemester = new List<BLL.MyStudentClass>();
            if (semester != "")
            {
                var studs = db.Students.ToList();

                foreach (var Std in studs)
                {
                    foreach (var data in Std.FK_CourseStudent)
                    {
                        if (data.Semester.StartsWith(semester))
                        {
                            var newStd = new BLL.MyStudentClass();
                            newStd.StudID = data.CourseId;
                            newStd.Name = data.Student.Name;
                            newStd.City = data.Student.City;
                            newStd.Phone = data.Student.Phone;
                            studentsForSelectedSemester.Add(newStd);
                        }
                    }
                }

                return Json(studentsForSelectedSemester);
            }
            else {
                studentsForSelectedSemester = null;
                return Json(studentsForSelectedSemester);
            }
        }

        public ActionResult MainTeacherReportsIndex()
        {
            ViewData["TeacherName"] = new SelectList(db.Teachers, "TeacherId", "Name");
            ViewData["CoursesAssigend"] = new SelectList("");
            return View();
        }

        //[HttpPost]
        //public JsonResult GetTeachers(int TeacherId)
        //{

        //}

	}
}