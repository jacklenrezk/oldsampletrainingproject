﻿using SchoolWebApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Xml.Linq;

namespace SchoolWebApplication.Controllers
{
    public class HomeController : Controller
    {
        private SchoolEntities2 db = new SchoolEntities2();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            var classes = db.Classes.ToList();
            //var classes = db.Classes.Include(x => x.Teacher);

            return View(classes);
        }

        
        public ActionResult MainPage(int? UserId,int? Flag)
        {
            if(Session["LoggedUserFullName"]!=null)
            {
                if(UserId !=null)
                {
                    var user = db.Users.Find(UserId);
                    Session["LoginUser"] = "true";
                    Session["LoggedUserID"] = user.UserId;
                    Session["LoggedUserFullName"] = user.FullName;
                }
                if (Flag != null)
                {
                    ViewBag.DoubleLogin = "You are already logged in as " + Session["LoggedUserFullName"];
                }
                else
                {
                    ViewBag.DoubleLogin = null;
                }
                int y = Convert.ToInt32(DateTime.Now.Year.ToString());
                var courses = db.Courses.ToList();
                ViewBag.Teachers = db.Teachers.ToList();
                ViewBag.Classes = db.Classes.ToList();
                ViewBag.Students = db.Students.ToList();
                return View(courses);
            }
            else
            {
                return RedirectToAction("MainLog", "Home");
            }
            
        }
        
        [HttpGet]
        public ActionResult MainRegister()
        {
            return View();
        }
        [HttpPost]
        public JsonResult MainRegister(string userNam, string fullNam, string phone, string email, string password, string confirmPassword)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var user = new User();
                    user.UserName = userNam;
                    user.FullName = fullNam;
                    user.Phone = phone;
                    user.Email = email;
                    user.Password = Shared.BLL.Encrypt(password);
                    user.ConfirmPassword = confirmPassword;
                    user.IsAdmin = 0;
                    user.RememberMe = 0;
                    db.Users.Add(user);
                    db.SaveChanges();
                    return Json(new
                    {
                        redirectUrl = Url.Action("MainLog", "Home"),
                        isRedirect = true
                    });
                }
                catch(Exception exception)
                {
                    return Json(new { status = false, message = exception.Message });
                }
            }
            var errors = string.Join(@"<br /> ", ModelState.Values
                                    .SelectMany(x => x.Errors)
                                    .Select(x => x.ErrorMessage));

            return Json(new { status = false, message = errors });
        }
        
        
        [HttpGet]
        public ActionResult MainLog()
        {
            HttpCookie UserId = Request.Cookies["RememberMeValue"];
            if (UserId != null)
            {
                int userid = Convert.ToInt32(UserId);
                return RedirectToAction("MainPage", "Home", new { userid });
            }
            if (Session["LoginUser"] != null && Session["LoggedUserID"] != null && Session["LoggedUserFullName"]!=null)
            {
                int userid = Convert.ToInt32(Session["LoggedUserID"]);
                int Flag = 1;
                return RedirectToAction("MainPage", "Home", new { userid ,Flag});
            }
            
            ViewBag.UserId = UserId;
            
            return View();
        }

        
        [HttpPost]
        public JsonResult MainLog(string UserName, string Password, bool RememberMe, string returnUrl)
        {
            try
            {
                if (UserName!="" && Password!="")
                {
                    //var users = db.Users.ToList();
                    
                    var user = db.Users.FirstOrDefault(u => u.UserName == UserName);
                    if (user == null) { return Json(new { status = false, message = "Not found User:" + UserName }); }
                    var hashedPassword = Shared.BLL.Encrypt(Password);
                    if (user.UserName == UserName && user.Password == hashedPassword)
                    {
                        if(RememberMe==true)
                        {
                            HttpCookie LoggedUserId = new HttpCookie("RememberMeValue");
                            LoggedUserId.Value = user.UserId.ToString();
                        }
                        //FormsAuthentication.SetAuthCookie(user.UserName, false);
                        //if (returnUrl != null)
                        //{
                        //    return Json(new { Url = returnUrl, isRedirect = true });
                        //    //return RedirectToAction("MainPage", "Home");
                        //}
                        Session["LoginUser"] = "true";
                        Session["LoggedUserID"] = user.UserId;
                        Session["LoggedUserFullName"] = user.FullName;
                        return Json(new
                        {
                            redirectUrl = Url.Action("MainPage", "Home"),
                            isRedirect = true
                        });

                    }
                    else
                    {
                        if (user.Password != hashedPassword) { return Json(new { status = false, message = "Wrong Password" }); }
                        
                        return Json(new { status = false, message = "Invalid username or password" });
                    }
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }

        }

        [HttpPost]
        public JsonResult AddCourse(string Title, int CreditHourse, int TeacherId, int ClassId)
        {
            try
            {
                if (Title != "" && CreditHourse != 0 && TeacherId != 0)
                {
                    var newcourse = new Course();
                    newcourse.Title = Title;
                    newcourse.CreditHours = CreditHourse;
                    newcourse.TeacherId = TeacherId;
                    if (ClassId != 0) { newcourse.ClassId = ClassId; }
                    db.Courses.Add(newcourse);
                    db.SaveChanges();

                    ViewBag.Teachers = db.Teachers.ToList();
                    ViewBag.Students = db.Students.ToList();
                    ViewBag.Classes = db.Classes.ToList();
                    
                    //return View("MainPage", classes);
                    var ObjectTeacher = db.Teachers.Find(TeacherId);

                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        
        public ActionResult MainGridPartialView()
        {
            ViewBag.Students = db.Students.ToList();
            var courses = db.Courses.ToList();
            return PartialView(courses);
        }

        [HttpPost]
        public JsonResult EditCourse(int CourseId, string Title, int CreditHours, int TeacherId, int ClassId)
        {
            try
            {
                if (Title != "" && CreditHours != 0 && TeacherId != 0)
                {
                    var newcourse = db.Courses.Find(CourseId);
                    newcourse.Title = Title;
                    newcourse.CreditHours = CreditHours;
                    newcourse.TeacherId = TeacherId;
                    if (ClassId != 0) newcourse.ClassId = ClassId;
                    db.Entry(newcourse).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteCourse(int CourseId)
        {
            try
            {
                if (CourseId != 0)
                {
                    var dataCourse = db.Courses.Find(CourseId);
                    //dataClass.ClassId = 0;
                    foreach (var item in dataCourse.FK_CourseStudent)
                    {
                        dataCourse.FK_CourseStudent.Remove(item);
                        db.SaveChanges();
                    }
                    
                    //foreach(var std in dataClass.FK_CourseStudent.ToList())
                    //{
                    //    dataClass.Students.Remove(std);
                    //    db.SaveChanges();
                    //}
                    db.Courses.Remove(dataCourse);
                    var ckeck = db.SaveChanges();
                    //if(ckeck)
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "please try again" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
            
        }

        [HttpGet]
        public JsonResult GetCourseData(int CourseId)
        {
            var SelectedCourse = db.Courses.Find(CourseId);
            
            Course courseInfo = new Course
            {
                Title = SelectedCourse.Title,
                CreditHours = SelectedCourse.CreditHours,
                TeacherId = SelectedCourse.TeacherId,
                ClassId=SelectedCourse.ClassId
            };
            return Json(courseInfo, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public JsonResult GetAssignedStudents(int ClassId)
        //{
        //    var choosesClass = db.Classes.Find(ClassId);
        //    string[] StudentsList=new string[choosesClass.Students.Count];
        //    int x = 0;
        //    foreach(var st in choosesClass.Students)
        //    {
        //        StudentsList[x] = st.Name+"\n";
        //        x++;
        //    }

        //    return Json(StudentsList, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult AssignStudentsPartialView()
        {
            var students = db.Students.ToList();
            
            return View(students);
        }

        //public JsonResult AssignStudents(int ClassId, int StudentId)
        //{
        //    try
        //    {
        //        if(ClassId!=0||StudentId!=0)
        //        {
        //            var SelectClass=db.Classes.Find(ClassId);
        //            var SelectStudent=db.Students.Find(StudentId);
        //            SelectClass.Students.Add(SelectStudent);
        //            db.SaveChanges();
        //            return Json(new { status = true });
        //        }
        //        return Json(new { status = true, message = "No selected student found" });
        //    }
        //    catch (Exception exception)
        //    {
        //        return Json(new { status = false, message = exception.Message });
        //    }
        //}

        
        public ActionResult MainLogOut()
        {
            FormsAuthentication.SignOut();
            Session["LoginUser"] = "false";
            Session["LoggedUserFullName"] = null;
            Session["LoggedUserID"] = null;
            return RedirectToAction("MainLog");
        }

        [HttpPost]
        public JsonResult CheckSessionNull()
        {
            try
            {
                if (Session["LoginUser"] != "false" && Session["LoggedUserFullName"] != null && Session["LoggedUserID"] != null)
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("MainPage", "Home"),
                        isRedirect = true
                    });
                }
                return Json(new { status = true });
            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        [HttpPost]
        public JsonResult CheckSession()
        {
            try
            {
                if (Session["LoginUser"]=="false" && Session["LoggedUserFullName"] == null && Session["LoggedUserID"] == null)
                {
                    return Json(new
                    {
                        redirectUrl = Url.Action("MainLog", "Home"),
                        isRedirect = true
                    });
                }
                return Json(new { status = true });
            }
            catch(Exception exception) {
                return Json(new { status = false, message = exception.Message });
            }
        }

    }
}