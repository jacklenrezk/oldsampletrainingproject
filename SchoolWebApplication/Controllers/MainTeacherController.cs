﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolWebApplication.Models;
using System.Data.Entity;

namespace SchoolWebApplication.Controllers
{
    public class MainTeacherController : Controller
    {
        private SchoolEntities2 db = new SchoolEntities2();
        //
        // GET: /MainTeacher/
        public ActionResult MainTeachersIndex(int? UserId, int? Flag)
        {
            if (Session["LoggedUserFullName"] != null)
            {
                if (UserId != null)
                {
                    var user = db.Users.Find(UserId);
                    Session["LoginUser"] = "true";
                    Session["LoggedUserID"] = user.UserId;
                    Session["LoggedUserFullName"] = user.FullName;
                }
                if (Flag != null)
                {
                    ViewBag.DoubleLogin = "You are already logged in as " + Session["LoggedUserFullName"];
                }
                else
                {
                    ViewBag.DoubleLogin = null;
                }
                var teachers = db.Teachers.ToList();
                return View(teachers);
            }
            else
            {
                return RedirectToAction("MainLog", "Home");
            }
            
        }

        [HttpPost]
        public JsonResult AddTeacher(string Name, string Age, string Phone, string Address)
        {
            try
            {
                if (Name != "" && Age != "" && Phone != "" && Address!="")
                {
                    var newTeacher = new Teacher();
                    newTeacher.Name = Name;
                    newTeacher.Age = Age;
                    newTeacher.Phone = Phone;
                    newTeacher.Address = Address;
                    db.Teachers.Add(newTeacher);
                    db.SaveChanges();

                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        public ActionResult MainTeachersGridPartialView()
        {

            var teachers = db.Teachers.ToList();
            return PartialView(teachers);
        }

        [HttpPost]
        public JsonResult EditTeacher(int teacherId, string Name, string Age, string Phone, string Address)
        {
            try
            {
                if (Name != "" && Age != "" && Phone != "" && Address != "")
                {
                    var NewTeacher = db.Teachers.Find(teacherId);
                    NewTeacher.Name = Name;
                    NewTeacher.Age = Age;
                    NewTeacher.Phone = Phone;
                    NewTeacher.Address = Address;
                    db.Entry(NewTeacher).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        [HttpGet]
        public JsonResult GetTeacherData(int TeacherId)
        {
            var SelectedTeacher = db.Teachers.Find(TeacherId);

            Teacher teacherInfo = new Teacher
            {
                Name = SelectedTeacher.Name,
                Age = SelectedTeacher.Age,
                Phone = SelectedTeacher.Phone,
                Address=SelectedTeacher.Address
            };
            return Json(teacherInfo, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteTeacher(int TeacherId)
        {
            try
            {
                if (TeacherId != 0)
                {
                    var dataTeacher = db.Teachers.Find(TeacherId);
        //            var teacher = db.Teachers.Include(p => p.Classes)
        //.SingleOrDefault(p => p.TeacherId == TeacherId);

        //            foreach (var classs in teacher.Classes.ToList())
        //                db.Classes.Remove(classs);

        //            db.SaveChanges();
                    foreach (var course in dataTeacher.Courses)
                    {
                        dataTeacher.Courses.Remove(course);
                        db.SaveChanges();
                    }
                    db.Teachers.Remove(dataTeacher);
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "please try again" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }
	}
}