﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using SchoolWebApplication.Models;

namespace SchoolWebApplication.Controllers
{
    public class MainClassController : Controller
    {
        private SchoolEntities2 db = new SchoolEntities2();
        //
        // GET: /MainClass/
        public ActionResult MainClassesIndex(int? UserId, int? Flag)
        {
            try {
                if (Session["LoggedUserFullName"] != null)
                {
                    if (UserId != null)
                    {
                        var user = db.Users.Find(UserId);
                        Session["LoginUser"] = "true";
                        Session["LoggedUserID"] = user.UserId;
                        Session["LoggedUserFullName"] = user.FullName;
                    }
                    if (Flag != null)
                    {
                        ViewBag.DoubleLogin = "You are already logged in as " + Session["LoggedUserFullName"];
                    }
                    else
                    {
                        ViewBag.DoubleLogin = null;
                    }
                    var classes = db.Classes.ToList();
                    return View(classes);
                }
                else
                {
                    return RedirectToAction("MainLog", "Home");
                }
                
            }
            catch(Exception e) {
                return View(e);
            }
            
        }

        [HttpPost]
        public JsonResult AddClass(string Title,int ClassRoomSeats)
        {
            try
            {
                if (Title != "" && ClassRoomSeats != 0 )
                {
                    var newClass = new Class();
                    newClass.Title = Title;
                    newClass.StudentsNum = ClassRoomSeats;
                    //if (GPA > 4 || GPA < 0) { return Json(new { status = false, message = "Invalid GPA number" }); } else { newStudent.GPA = GPA; }

                    db.Classes.Add(newClass);
                    db.SaveChanges();

                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }

        }

        public ActionResult MainClassesGridPartialView()
        {
            
            var classes = db.Classes.ToList();
            return PartialView(classes);
        }

        [HttpPost]
        public JsonResult Editclass(int classId, string Title, int ClassRoomSeats)
        {
            try
            {
                if (classId != 0 && Title != "" && ClassRoomSeats != 0)
                {
                    var NewClass = db.Classes.Find(classId);
                    NewClass.Title = Title;
                    NewClass.StudentsNum = ClassRoomSeats;
                    db.Entry(NewClass).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "Invalid Entry" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }

        [HttpGet]
        public JsonResult GetClassData(int ClassId)
        {
            var SelectedClass = db.Classes.Find(ClassId);

            Class classInfo = new Class
            {
                Title = SelectedClass.Title,
                StudentsNum = SelectedClass.StudentsNum
            };
            return Json(classInfo, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult DeleteClass(int ClassId)
        {
            try
            {
                if (ClassId != 0)
                {
                    var dataClass = db.Classes.Find(ClassId);
                    foreach(var item in dataClass.Courses)
                    {
                        dataClass.Courses.Remove(item);
                    }
                    db.Classes.Remove(dataClass);
                    db.SaveChanges();
                    return Json(new { status = true });
                }
                else
                {
                    return Json(new { status = false, message = "please try again" });
                }

            }
            catch (Exception exception)
            {
                return Json(new { status = false, message = exception.Message });
            }
        }
	

	}
}