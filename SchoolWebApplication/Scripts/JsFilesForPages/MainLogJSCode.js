﻿$(document).ready(function () {
    CheckingSessionNull();
});

function CheckingSessionNull() {
    $.ajax({
        type: "POST",
        url: '/Home/CheckSessionNull',
        datatype: "json",
        data: {},
        success: function (json) {
            if (json.isRedirect) {
                window.location.href = json.redirectUrl;
            } else {
                return false;
            }
        },
        error: function (json) {
            alert(json.error);
        }
    });
}

function LoginFunction() {

    var UserName = document.getElementById('UserName').value;
    var Password = document.getElementById('Password').value;
    if ($("#remember").is(':checked'))
    { var Remember = true; }
    else { var Remember = false; }
    //alert('@Url.Action("MainLog")');
    if (UserName == "" || Password == "") { $("#passwordValidationMessage").html("Please enter your username\Password"); return; }
    $.ajax({
        url: '/Home/MainLog',
        type: "POST",
        dataType: "json",
        data: { UserName: UserName, Password: Password, RememberMe: Remember},
        success: function (result) {
            if (result.status == false) {
                $("#passwordValidationMessage").html(result.message); //id="passwordValidationMessage"
                //document.getElementById('passwordValidationMessage').innerText('Wrong Password');
            } else {
                if (result.isRedirect) {
                    //window.location.href = result.Url;
                    window.location.href = result.redirectUrl;
                }
            }
        },
        error: function (result) {
            alert("faild");
            //document.getElementById('userNameValidation').innerHTML = result.statusText.toString();
            //alert(response.statusText);
            //alert(response.responseText);
        }

    }

            );
}