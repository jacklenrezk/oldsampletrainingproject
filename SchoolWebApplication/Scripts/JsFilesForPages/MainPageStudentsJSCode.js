﻿$(document).ready(function () {
    CheckingSession();


});

function CheckingSession() {
    $.ajax({
        type: "POST",
        url: '/Home/CheckSession',
        datatype: "json",
        data: {},
        success: function (json) {
            if (json.isRedirect) {
                window.location.href = json.redirectUrl;
            } else {
                return false;
            }
        },
        error: function (json) {
            alert(json.error);
        }
    });
}

function GetStudentData(studentid) {

    var url = "/MainStudent/GetStudentData";

    $.getJSON(url, { StudentId: studentid }, function (studentInfo) {
        $('#studentNameTxt').val(studentInfo.Name);
        $('#studentCityTxt').val(studentInfo.City);
        $('#studentPhoneTxt').val(studentInfo.Phone);

    });


    localStorage.setItem("studentIdForEdit", studentid);
    localStorage.setItem("EditOrAddFlag", "edit");
}

function AddStudentData() {
    $('#studentNameTxt').val("");
    $('#studentCityTxt').val("");
    $('#studentPhoneTxt').val("");
    $("#validationMessage").html('');
    localStorage.setItem("EditOrAddFlag", "add");
}



$('#submitSaveStudent').on('click', function () {
    //$("#action_progress").hidden = false;
    var flag = localStorage.getItem("EditOrAddFlag");
    if (flag == "add") {
        $('#submitSaveStudent').disabled = true;
        var Name = $('#studentNameTxt').val();
        var City = $('#studentCityTxt').val();
        var Phone = $('#studentPhoneTxt').val();
        

        $.ajax({
            type: "POST",
            url: '/MainStudent/AddStudent',
            dataType: "json",
            data: { Name: Name, City: City, Phone: Phone },
            success: function (result) {
                if (result.status == false) {
                    localStorage.clear;
                    $("#validationMessage").html(result.message);
                    //$("#action_progress").hidden = true;
                }
                else {

                    if (result.status == true) {
                        $('#modal-form').hide();
                        $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                        $('body').removeClass('modal-open');
                        //$('body').removeAttr('class');
                        //$('body').removeAttr('style');
                        //$('body').resize();
                        //$('body').attr('style', 'padding-right: 17px;');

                        //$('.modal.in').modal('hide');
                        //$('.modal-backdrop').attr('style', 'opacity:0;z-index: -1;');
                        //$('body').removeClass('modal-open');
                        
                        //$(".modal-backdrop").hide();
                        //$('body').removeClass('modal-open');

                        //$('.modal-backdrop').attr('style', 'opacity:0;z-index: -1;padding-right: 51px;');
                        //$('.modal-backdrop').attr('style', 'aria-hidden:true;opacity:0;z-index: -1;').off('click.dismiss.modal');
                        //$('.modal-backdrop').attr('style', 'keyboard: false;backdrop: static;');
                        //$('.modal-backdrop').attr('style', 'opacity:0; position: static;z-index: -1;');

                        $('#MainGrid').load('/MainStudent/MainStudentsGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }
        });

    } else {
        if (flag == "edit") {
            $('#submitSaveStudent').disabled = true;
            var Name = $('#studentNameTxt').val();
            var City = $('#studentCityTxt').val();
            var Phone = $('#studentPhoneTxt').val();
            
            var studentId = localStorage.getItem("studentIdForEdit");
            $.ajax({
                type: "POST",
                url: '/MainStudent/Editstudent',
                dataType: "json",
                data: { StudentId: studentId, Name: Name, City: City, Phone: Phone},
                success: function (result) {
                    if (result.status == false) {
                        localStorage.clear;
                        $("#validationMessage").html(result.message);

                    } else {
                        if (result.status == true) {
                            $('#modal-form').hide(true);
                            $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                            $('body').removeClass('modal-open');
                            

                            //$('.modal.in').modal('hide');
                            //$(".modal-backdrop").hide();
                            //$('body').removeClass('modal-open');

                            //$('.modal-backdrop').attr('style', 'aria-hidden:true;opacity:0;z-index: -1;').off('click.dismiss.modal');
                            //$('.modal-backdrop').attr('style', 'aria-hidden:true;opacity:0;z-index: -1;padding-right: 51px;');
                            //$('.modal-backdrop').attr('style', 'keyboard: false;backdrop: static;');
                            //$('.modal-backdrop').attr('style', 'opacity:0; position: static;z-index: -1;');

                            $('#MainGrid').load('/MainStudent/MainStudentsGridPartialView');
                        }
                    }
                },
                error: function (result) {
                    $("#validationMessage").html(result.message);
                }

            });
        }
    }

});

debugger
function deleteStudent(studentId) {
    if (confirm("Are you sure you want to delete this Student?")) {
        $.ajax({
            type: "POST",
            url: '/MainStudent/DeleteStudent',
            dataType: "json",
            data: { StudentId: studentId },
            success: function (result) {
                if (result.status == false) {
                    alert(result.message);
                    //$("#validationMessage").html(result.message);

                } else {
                    if (result.status == true) {
                        $('#MainGrid').load('/MainStudent/MainStudentsGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }

        });

    }
}