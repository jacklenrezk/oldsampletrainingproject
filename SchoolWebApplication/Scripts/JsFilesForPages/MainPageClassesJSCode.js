﻿$(document).ready(function () {
    CheckingSession();


});

function CheckingSession() {
    $.ajax({
        type: "POST",
        url: '/Home/CheckSession',
        datatype: "json",
        data: {},
        success: function (json) {
            if (json.isRedirect) {
                window.location.href = json.redirectUrl;
            } else {
                return false;
            }
        },
        error: function (json) {
            alert(json.error);
        }
    });
}

function GetClassData(classid) {

    var url = "/MainClass/GetClassData";

    $.getJSON(url, { ClassId: classid }, function (ClassInfo) {
        $('#classTitleTxt').val(ClassInfo.Title);
        $('#classRoomseatTxt').val(ClassInfo.StudentsNum);

    });


    localStorage.setItem("classIdForEdit", classid);
    localStorage.setItem("EditOrAddFlag", "edit");
}

function AddClassData() {
    $('#classTitleTxt').val("");
    $('#classRoomseatTxt').val("");
    $("#validationMessage").html('');
    localStorage.setItem("EditOrAddFlag", "add");
}



$('#submitSaveClass').on('click', function () {
    //$("#action_progress").hidden = false;
    var flag = localStorage.getItem("EditOrAddFlag");
    if (flag == "add") {
        $('#submitSaveClass').disabled = true;
        var Title = $('#classTitleTxt').val();
        var ClassRoomSeats = $('#classRoomseatTxt').val();


        $.ajax({
            type: "POST",
            url: '/MainClass/AddClass',
            dataType: "json",
            data: { Title: Title, ClassRoomSeats: ClassRoomSeats },
            success: function (result) {
                if (result.status == false) {
                    localStorage.clear;
                    $("#validationMessage").html(result.message);
                    //$("#action_progress").hidden = true;
                }
                else {

                    if (result.status == true) {
                        $('#modal-form').hide();
                        $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                        $('body').removeClass('modal-open');
                        

                        $('#MainGrid').load('/MainClass/MainClassesGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }
        });

    } else {
        if (flag == "edit") {
            $('#submitSaveClass').disabled = true;
            var Title = $('#classTitleTxt').val();
            var ClassRoomSeats = $('#classRoomseatTxt').val();

            var classId = localStorage.getItem("classIdForEdit");
            $.ajax({
                type: "POST",
                url: '/MainClass/Editclass',
                dataType: "json",
                data: { classId: classId, Title: Title, ClassRoomSeats: ClassRoomSeats },
                success: function (result) {
                    if (result.status == false) {
                        localStorage.clear;
                        $("#validationMessage").html(result.message);

                    } else {
                        if (result.status == true) {
                            $('#modal-form').hide(true);
                            $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                            $('body').removeClass('modal-open');


                            //$('.modal.in').modal('hide');
                            //$(".modal-backdrop").hide();
                            //$('body').removeClass('modal-open');

                            //$('.modal-backdrop').attr('style', 'aria-hidden:true;opacity:0;z-index: -1;').off('click.dismiss.modal');
                            //$('.modal-backdrop').attr('style', 'aria-hidden:true;opacity:0;z-index: -1;padding-right: 51px;');
                            //$('.modal-backdrop').attr('style', 'keyboard: false;backdrop: static;');
                            //$('.modal-backdrop').attr('style', 'opacity:0; position: static;z-index: -1;');

                            $('#MainGrid').load('/MainClass/MainClassesGridPartialView');
                        }
                    }
                },
                error: function (result) {
                    $("#validationMessage").html(result.message);
                }

            });
        }
    }

});

debugger
function deleteClass(classId) {
    if (confirm("Are you sure you want to delete this Class?")) {
        $.ajax({
            type: "POST",
            url: '/MainClass/DeleteClass',
            dataType: "json",
            data: { ClassId: classId },
            success: function (result) {
                if (result.status == false) {
                    alert(result.message);
                    //$("#validationMessage").html(result.message);

                } else {
                    if (result.status == true) {
                        $('#MainGrid').load('/MainClass/MainClassesGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }

        });

    }
}