﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using SchoolWebApplication.Models;

namespace SchoolWebApplication.Shared
{
    public class BLL
    {
        public class MyCourseClass
        {
            public int CourseId { get; set; }
            public string Title { get; set; }
            public int CreditHours { get; set; }
            public string TeacherName { get; set; }
            public string ClassTitle { get; set; }
            //public List<Student> AssignedStudents { get; set; }
        }

        public class MyStudentClass
        {
            public int StudID { get; set; }
            public string Name { get; set; }
            public string City { get; set; }
            public string Phone { get; set; }
            public string CumulativeGPA { get; set; }
        }

        public class StudentTranscript
        {
            public int StudId { get; set; }
            public string Course { get; set; }
            public int CreditHours { get; set; }
            public int Grade { get; set; }
            public decimal GradePoints { get; set; }

        }

        public class MyTeacherClass
        {
            public int TeachId { get; set; }
            public string Name { get; set; }
            public int Age { get; set; }
            public string Phone { get; set; }
            public string Address { get; set; }
        }

        public class CourseDetails
        {
            public string CourseTitle { get; set; }
            public decimal CourseGrade { get; set; }
            public string Semester { get; set; }
            public string ClassForCourse { get; set; }
            public string TeacherForCourse { get; set; }
        }

            public static string Encrypt(string source)
            {
                //string source = "Hello World!";
                using (MD5 md5Hash = MD5.Create())
                {
                    string hash = GetMd5Hash(md5Hash, source);

                    //Console.WriteLine("The MD5 hash of " + source + " is: " + hash + ".");

                    //Console.WriteLine("Verifying the hash...");

                    if (VerifyMd5Hash(md5Hash, source, hash))
                    {
                        return hash;
                    }
                    else
                    {
                        return "false";
                    }
                }



            }
            static string GetMd5Hash(MD5 md5Hash, string input)
            {

                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }

            // Verify a hash against a string.
            static bool VerifyMd5Hash(MD5 md5Hash, string input, string hash)
            {
                // Hash the input.
                string hashOfInput = GetMd5Hash(md5Hash, input);

                // Create a StringComparer an compare the hashes.
                StringComparer comparer = StringComparer.OrdinalIgnoreCase;

                if (0 == comparer.Compare(hashOfInput, hash))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        
    }
}