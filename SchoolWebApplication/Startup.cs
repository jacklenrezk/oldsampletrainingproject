﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SchoolWebApplication.Startup))]
namespace SchoolWebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
