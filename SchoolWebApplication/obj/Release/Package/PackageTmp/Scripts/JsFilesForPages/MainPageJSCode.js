﻿

$(document).ready(function () {
    CheckingSession();
    
    //alert(session.getAttribute("LoggedUserFullName"));
    var username=  Session["LoggedUserFullName"];
    var value = '@Request.RequestContext.HttpContext.Session["LoggedUserFullName"]';
    alert(username);
});

function CheckingSession() {
    $.ajax({
        type: "POST",
        url: '/Home/CheckSession',
        datatype: "json",
        data: {},
        success: function (json) {
            if (json.isRedirect) {
                window.location.href = json.redirectUrl;
            } else {
                return false;
            }
        },
            error:function(json){
                alert(json.error);
            }
    });
}

function EditCourseData(courseId) {

    var url = "/Home/GetCourseData";
        
    $.getJSON(url, { CourseId: courseId }, function (courseInfo) {
        $('#courseTitleTxt').val(courseInfo.Title);
        $('#courseCredHoursTxt').val(courseInfo.CreditHours);
        $('#teacher_SelectedIdTxt').val(courseInfo.TeacherId);
        $('#class_SelectedIdTxt').val(courseInfo.ClassId);

    });
       

    localStorage.setItem("courseIdForEdit", courseId);
    localStorage.setItem("EditOrAddFlag", "edit");
}
function AddCourseData() {
    $('#courseTitleTxt').val("");
    $('#courseCredHoursTxt').val("");
    $("#validationMessage").html('');
    localStorage.setItem("EditOrAddFlag", "add");
}



$('#submitSave').on('click', function () {
    //$("#action_progress").hidden = false;
    var flag = localStorage.getItem("EditOrAddFlag");
    if (flag == "add") {
        $('#submitSave').disabled = true;
        var title = $('#courseTitleTxt').val();
        var CreditHourse = $('#courseCredHoursTxt').val();
        var teacherId = $("#teacher_SelectedIdTxt").children(":selected").attr("value");
        var ClassId = $("#class_SelectedIdTxt").children(":selected").attr("value")

        //var classesTable = $('#MainTable').
        //.dataTable({
        //    "bJQueryUI": true,
        //    "bServerSide": true,
        //    "sAjaxSource": "/Home/MainGridPartialView"
        //});

        $.ajax({
            type: "POST",
            url: '/Home/AddCourse',
            dataType: "json",
            data: { Title: title, CreditHourse: CreditHourse, TeacherId: teacherId,ClassId:ClassId },
            success: function (result) {
                //alert("Entered");
                if (result.status == false) {
                    localStorage.clear;
                    $("#validationMessage").html(result.message);
                    //$("#action_progress").hidden = true;
                }
                else {

                    if (result.status == true) {
                        $('#modal-form').hide(true);
                        $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                        $('body').removeClass('modal-open');

                        //$('#modal-form').removeClass("");
                        
                        
                        //$('.modal-backdrop').remove();
                        $('#MainGrid').load('/Home/MainGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }
        });

    } else {
        if (flag == "edit") {
            $('#submitSave').disabled = true;
            var title = $('#courseTitleTxt').val();
            var CreditHours = $('#courseCredHoursTxt').val();
            var teacherId = $("#teacher_SelectedIdTxt").children(":selected").attr("value");
            var classId = $("#class_SelectedIdTxt").children(":selected").attr("value");
            var courseId = localStorage.getItem("courseIdForEdit");
            $.ajax({
                type: "POST",
                url: '/Home/EditCourse',
                dataType: "json",
                data: { CourseId: courseId, Title: title, CreditHours: CreditHours, TeacherId: teacherId, ClassId: classId },
                success: function (result) {
                    if (result.status == false) {
                        localStorage.clear;
                        $("#validationMessage").html(result.message);

                    } else {
                        if (result.status == true) {
                            $('#modal-form').hide(true);
                            $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                            $('body').removeClass('modal-open');
                            //$('.modal-backdrop').remove();
                            $('#MainGrid').load('/Home/MainGridPartialView');
                        }
                    }
                },
                error: function (result) {
                    $("#validationMessage").html(result.message);
                }

            });
        }
    }

});

function deleteCourse(value) {
    if (confirm("Are you sure you want to delete this Class?")) {
        $.ajax({
            type: "POST",
            url: '/Home/DeleteCourse',
            dataType: "json",
            data: { CourseId: value },
            success: function (result) {
                if (result.status == false) {
                    alert(result.message);
//                    $("#validationMessage").html(result.message);

                } else {
                    if (result.status == true) {
                        //$("#modal-form").modal("hide");
                        $('#MainGrid').load('/Home/MainGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }

        });

    }
}
function AssignStudents(classId) {
    localStorage.setItem("AssignedClassId", classId);

    $('#content' + classId).animate({
        height: 'toggle'
    });
    
}
function AddStudentsDiv()
{
    var classId = localStorage.getItem("AssignedClassId");
    var studentId = $('#student_idTxt' + classId).children(":selected").attr("value");
    //var studentId = $("#student_idTxt option:selected").attr("value");
    
    $.ajax({
        type: "POST",
        url: '/Home/AssignStudents',
        dataType: "json",
        data: { ClassId: classId, StudentId: studentId },
        success: function (result) {
            if (result.status == false) {
                $("#AssignStudentsValidationMsg").html(result.message);
                //$("#action_progress").hidden = true;

            } else {
                if (result.status == true) {
                    //$("#action_progress").hidden = true;
                    //location.reload();
                    $('#MainGrid').load('/Home/MainGridPartialView');
                }
            }
        },
        error: function (result) {
            $("#AssignStudentsValidationMsg").html(result.message);
            //$("#action_progress").hidden = true;
        }

    });
}

//function ShowAssignedStudents(classid)
//{
//    $('#AssignedStudentsTable').html(' ');
//    var url = "/Home/GetAssignedStudents";
//    $.getJSON(url, { ClassId: classid }, function (StudentsList) {
        
        
//        for(st in StudentsList){

//            var $row = $('<tr>' + '<td>' + StudentsList[st] + '</td>' + '</tr>');
//            $('#AssignedStsDiv' + classid).append($row);
//        }
//        toogling(classid);
//    });

//    //$('#AssignedStsDiv' + classid).animate({
//    //    height:'toggle'
//    //});
//}

function ShowAssignedStudentss(classid)
{
    //$("#PopOverBtn" + classid).on('click', function () {
        var stlist = [];
        var url = "/Home/GetAssignedStudents";
        $.getJSON(url, { ClassId: classid }, function (StudentsList) {
            stlist.push(null);
            for (st in StudentsList) {
                stlist.push(StudentsList[st]);
            }
            $('[data-toggle="popover"]').popover({ title: "Students", content: stlist, });
            //$('[data-toggle="popover"]').data('popover', null).popover(true);
            //$('[data-toggle="popover"]').popover().contents.valueOf() = null;
            //$('[data-toggle="popover"]').popover({ title: "Students", content: , });
        });
    

        //$('#PopOverBtn'+classid).popover({
        //    trigger: 'manual',
        //    placement: 'right',
        //    content: function () {
        //        var stlist = [];
        //        var url = "/Home/GetAssignedStudents";
        //        $.getJSON(url, { ClassId: classid }, function (StudentsList) {
        //            stlist.push(null);
        //            for (st in StudentsList) {
        //                stlist.push(StudentsList[st]);
        //            }
                    
        //            return stlist;
        //            //$('[data-toggle="popover"]').popover({ title: "Students", content: stlist, });
        //        });
        //        return stlist;
        //    }
            
        //});
        //$('#PopOverBtn' + classid).popover("show");

    ///////////
    
}


function toogling(classid)
{
    //$('#MainGrid').load('/Home/MainGridPartialView');
    
    $('#AssignedStsDiv' + classid).animate({
        height:'toggle'
    });
}


