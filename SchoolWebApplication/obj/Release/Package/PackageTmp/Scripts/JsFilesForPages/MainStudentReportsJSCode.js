﻿

$("#AllStudents").change(function () {

    $("#ReportsStudentsDataTable").empty();

        $.ajax({

            type: 'POST',

            url: '/MainReports/GetStudents',

            dataType: 'json',

            data: { StudentId: $("#AllStudents").val() },

            success: function (data) {
                if (data.StudentDetails == null) {
                    $("#ReportsStudentsDataTable").append('<tr><td> No Data Found </td></tr>');
                    $("#CoursesTitles").empty();
                    $("#CoursesTitles").append('<option> Select Course </option>');
                } else {
                    $("#CoursesTitles").empty();
                    $("#ReportsStudentsDataTable").append('<tr><td>Student Name</td>   <td>City</td>    <td>Phone</td> </tr>');
                    $("#ReportsStudentsDataTable").append('<tr><td>' + data.StudentDetails.Name + '</td><td>' + data.StudentDetails.City + '</td><td>' + data.StudentDetails.Phone + '</td></tr>');
                    if (data.StudentDetailsList.length > 0) {
                        $("#ReportsAssignedCoursesDataTable").empty();
                        $("#ReportsAssignedCoursesDataTable").append('<tr><td>Course Title</td>   <td>Course Grade</td>    <td>Semester</td> <td>Class</td> <td>Teacher</td> </tr>');
                        $.each(data.StudentDetailsList, function (i, itm) {
                            $("#ReportsAssignedCoursesDataTable").append('<tr><td>' + itm.CourseTitle + '</td><td>' + itm.CourseGrade + '</td><td>' + itm.Semester + '</td><td>' + itm.ClassForCourse + '</td><td>' + itm.TeacherForCourse + '</td></tr>');
                        });
                    }
                    else {
                        $("#ReportsAssignedCoursesDataTable").empty();
                        $("#ReportsAssignedCoursesDataTable").append('<tr><td>this student is not assigned to courses yet</td></tr>')
                    }
                    

                    $("#CoursesTitles").empty();
                    if (data.corsList != null) {
                        $.each(data.corsList, function (i, Cors) {
                            //alert(CourseDetails.AssignedStudents[i]);
                            $("#CoursesTitles").append('<option value="' + Cors.Value + '">' +
                             Cors.Text + '</option>');
                        });
                    }
                }

            },

            error: function (ex) {

                alert('Failed to retrieve Courses.' + ex);

            }

        });

        //return false;

    });

$("#CoursesTitles").change(function () {

    $("#ReportsStudentsDataTable").empty();
    $("#ReportsAssignedCoursesDataTable").empty();
    

        $.ajax({

            type: 'POST',

            url: '/MainReports/GetStudentForCourse',

            dataType: 'json',

            data: { CourseId: $("#CoursesTitles").val() },

            success: function (StudListForSelectedCourse) {
                if (StudListForSelectedCourse.length == 0) {
                    $("#ReportsStudentsDataTable").append('<tr><td> No Students Assigned </td></tr>');
                    
                } else {
                    $("#ReportsStudentsDataTable").empty();
                    $("#ReportsStudentsDataTable").append('<tr><td>Student Name</td>   <td>City</td>    <td>Phone</td> </tr>');
                    $.each(StudListForSelectedCourse, function (i, Stud) {

                        $("#ReportsStudentsDataTable").append('<tr><td>' + Stud.Name + '</td><td>' + Stud.City + '</td><td>' + Stud.Phone + '</td></tr>');

                    });
                }

            },

            error: function (ex) {

                alert('Failed to retrieve Courses.' + ex);

            }

        });


});

function GetSemesterStudents() {

    var SearchVal = $("SemesterSearchInput").val();
    if (SearchVal != undefined || SearchVal != "") {

        $.ajax({
            type: 'POST',

            url: '/MainReports/GetDataForSearchedSemester',

            dataType: 'json',

            data: { semester: $("#SemesterSearchInput").val() },

            success: function (studentsForSelectedSemester) {
                if (studentsForSelectedSemester.length == 0) {
                    $("#ReportsStudentsDataTable").empty();
                    $("#ReportsAssignedCoursesDataTable").empty();
                    $("#ReportsStudentsDataTable").append('<tr><td> No Students Assigned to this Semester </td></tr>');
                } else {
                    $("#ReportsStudentsDataTable").empty();
                    $("#ReportsAssignedCoursesDataTable").empty();
                    $("#ReportsStudentsDataTable").append('<tr><td>Student Name</td>   <td>City</td>    <td>Phone</td> </tr>');
                    $.each(studentsForSelectedSemester, function (i, Std) {
                        $("#ReportsStudentsDataTable").append('<tr><td>' + Std.Name + '</td><td>' + Std.City + '</td><td>' + Std.Phone + '</td></tr>');
                    });
                }

            },
            error: function (ex) {
                $("#ReportsStudentsDataTable").empty();
                $("#ReportsAssignedCoursesDataTable").empty();
                //alert('Failed to retrieve Courses.' + ex);

            }
        });
    }

}