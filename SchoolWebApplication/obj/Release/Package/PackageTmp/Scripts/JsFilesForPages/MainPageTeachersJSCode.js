﻿$(document).ready(function () {
    CheckingSession();


});

function CheckingSession() {
    $.ajax({
        type: "POST",
        url: '/Home/CheckSession',
        datatype: "json",
        data: {},
        success: function (json) {
            if (json.isRedirect) {
                window.location.href = json.redirectUrl;
            } else {
                return false;
            }
        },
        error: function (json) {
            alert(json.error);
        }
    });
}

function GetTeacherData(teacherid) {

    var url = "/MainTeacher/GetTeacherData";

    $.getJSON(url, { TeacherId: teacherid }, function (teacherInfo) {
        $("#validationMessage").html('');
        $('#teacherNameTxt').val(teacherInfo.Name);
        $('#teacherAgeTxt').val(teacherInfo.Age);
        $('#teacherPhoneTxt').val(teacherInfo.Phone);
        $('#teacherAddresTxt').val(teacherInfo.Address)

    });


    localStorage.setItem("teacherIdForEdit", teacherid);
    localStorage.setItem("EditOrAddFlag", "edit");
}

function AddTeacherData() {
    $('#teacherNameTxt').val("");
    $('#teacherAgeTxt').val("");
    $('#teacherPhoneTxt').val("");
    $('#teacherAddresTxt').val("");
    $("#validationMessage").html('');

    localStorage.setItem("EditOrAddFlag", "add");
}

$('#submitSaveTeacher').on('click', function () {
    //$("#action_progress").hidden = false;
    var flag = localStorage.getItem("EditOrAddFlag");
    if (flag == "add") {
        $('#submitSaveTeacher').disabled = true;
        var Name = $('#teacherNameTxt').val();
        var Age = $('#teacherAgeTxt').val();
        var phone = $('#teacherPhoneTxt').val();
        var Address = $('#teacherAddresTxt').val();


        $.ajax({
            type: "POST",
            url: '/MainTeacher/AddTeacher',
            dataType: "json",
            data: { Name: Name, Age: Age, phone: phone,Address:Address },
            success: function (result) {
                if (result.status == false) {
                    localStorage.clear;
                    $("#validationMessage").html(result.message);
                    //$("#action_progress").hidden = true;
                }
                else {

                    if (result.status == true) {

                        $('#modal-form').hide(true);
                        $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                        $('body').removeClass('modal-open');
                        //$('.modal-backdrop').remove();
                        $('#MainGrid').load('/MainTeacher/MainTeachersGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }
        });

    } else {
        if (flag == "edit") {
            $('#submitSaveTeacher').disabled = true;
            var Name = $('#teacherNameTxt').val();
            var Age = $('#teacherAgeTxt').val();
            var Phone = $('#teacherPhoneTxt').val();
            var Address = $('#teacherAddresTxt').val();

            var teacherId = localStorage.getItem("teacherIdForEdit");
            $.ajax({
                type: "POST",
                url: '/MainTeacher/EditTeacher',
                dataType: "json",
                data: { TeacherId: teacherId, Name: Name, Age: Age, Phone: Phone, Address: Address },
                success: function (result) {
                    if (result.status == false) {
                        localStorage.clear;
                        $("#validationMessage").html(result.message);

                    } else {
                        if (result.status == true) {
                            $('#modal-form').hide(true);
                            $('.modal-backdrop').attr('style', 'opacity:0 !important;z-index: -1;position: static!important;');
                            $('body').removeClass('modal-open');
                            //$('.modal-backdrop').remove();
                            $('#MainGrid').load('/MainTeacher/MainTeachersGridPartialView');
                        }
                    }
                },
                error: function (result) {

                    alert(result.message);
                }

            });
        }
    }

});

function DeleteTeacher(teacherId) {
    if (confirm("Are you sure you want to delete this Teacher?")) {
        $.ajax({
            type: "POST",
            url: '/MainTeacher/DeleteTeacher',
            dataType: "json",
            data: { TeacherId: teacherId },
            success: function (result) {
                if (result.status == false) {
                    alert(result.message);
//                    $("#validationMessage").html(result.message);

                } else {
                    if (result.status == true) {
                        //$("#modal-form").modal("hide");
                        $('#MainGrid').load('/MainTeacher/MainTeachersGridPartialView');
                    }
                }
            },
            error: function (result) {
                $("#validationMessage").html(result.message);
            }

        });

    }
}