﻿

$("#CoursesTiltles").change(function () {

    $("#ReportsCoursesDataTable").empty();

        $.ajax({

            type: 'POST',

            url: '/MainReports/GetCourses',

            dataType: 'json',

            data: { courseId: $("#CoursesTiltles").val() },

            success: function (data) {
                if (data.CourseDetails == null) {
                    $("#ReportsCoursesDataTable").append('<tr><td> No Data Found </td></tr>');
                    $("#StudentsValues").empty();
                    $("#StudentsValues").append('<option> Select Class </option>');
                } else {
                    
                    $("#ReportsCoursesDataTable").append('<tr><td>Course Title</td>   <td>Credit Hours</td>    <td>Course Teacher</td> <td>Course Class</td></tr>');
                    $("#ReportsCoursesDataTable").append('<tr><td>' + data.CourseDetails.Title + '</td><td>' + data.CourseDetails.CreditHours + '</td><td>' + data.CourseDetails.TeacherName + '</td><td>' + data.CourseDetails.ClassTitle + '</td></tr>');
                    $("#StudentsValues").empty();
                    if (data.stList != null) {
                        $.each(data.stList, function (i, Stud) {
                            //alert(CourseDetails.AssignedStudents[i]);
                            $("#StudentsValues").append('<option value="' + Stud.Value + '">' +
                             Stud.Text + '</option>');
                        });
                    }
                }

            },

            error: function (ex) {

                alert('Failed to retrieve Courses.' + ex);

            }

        });

        //return false;

    });

$("#StudentsValues").change(function () {

    $("#ReportsCoursesDataTable").empty();

        $.ajax({

            type: 'POST',

            url: '/MainReports/GetStudentDetails',

            dataType: 'json',

            data: { StudentId: $("#StudentsValues").val() },

            success: function (coursesForSelectedStudent) {
                if (coursesForSelectedStudent.length == 0) {
                    $("#ReportsCoursesDataTable").append('<tr><td> No Students Assigned </td></tr>');
                    
                } else {
                    $("#ReportsCoursesDataTable").empty();
                    $("#ReportsCoursesDataTable").append('<tr><td>Course Title</td>   <td>Credit Hours</td>    <td>Course Teacher</td> <td>Course Class</td></tr>');
                    $.each(coursesForSelectedStudent, function (i, Cors) {

                        $("#ReportsCoursesDataTable").append('<tr><td>' + Cors.Title + '</td><td>' + Cors.CreditHours + '</td><td>' + Cors.TeacherName + '</td><td>' + Cors.ClassTitle + '</td></tr>');

                    });
                }

            },

            error: function (ex) {

                alert('Failed to retrieve Courses.' + ex);

            }

        });


});

function GetCreditHoursVals() {

    var SearchVal = $("CreditHoursSearchInput").val();
    if (SearchVal != undefined || SearchVal != "") {

        $.ajax({
            type: 'POST',

            url: '/MainReports/GetDataForSearchedCreditHours',

            dataType: 'json',

            data: { val: $("#CreditHoursSearchInput").val() },

            success: function (coursesForSelectedCreditHours) {
                if (coursesForSelectedCreditHours.length == 0) {
                    $("#ReportsCoursesDataTable").empty();
                    $("#ReportsCoursesDataTable").append('<tr><td> No Courses Found with selected Credit Hours </td></tr>');
                } else {
                    $("#ReportsCoursesDataTable").empty();
                    $("#ReportsCoursesDataTable").append('<tr><td>Course Title</td>   <td>Credit Hours</td>    <td>Course Teacher</td> <td>Course Class</td></tr>');
                    $.each(coursesForSelectedCreditHours, function (i, Cors) {
                        $("#ReportsCoursesDataTable").append('<tr><td>' + Cors.Title + '</td><td>' + Cors.CreditHours + '</td><td>' + Cors.TeacherName + '</td><td>' + Cors.ClassTitle + '</td></tr>');
                    });
                }

            },
            error: function (ex) {
                //alert('Failed to retrieve Courses.' + ex);

            }
        });
    }

}